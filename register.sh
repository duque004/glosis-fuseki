#!/bin/sh

# Check arguments and set local variables
if [ -z "$1" ]
  then
    echo "Please provide a new tag as first argument"
    exit -1
fi

# Compile 
docker login docker-registry.wur.nl
docker build -t docker-registry.wur.nl/duque004/glosis-fuseki .

# Push image to GitLab registry
docker tag docker-registry.wur.nl/duque004/glosis-fuseki docker-registry.wur.nl/duque004/glosis-fuseki:$1
docker push docker-registry.wur.nl/duque004/glosis-fuseki:$1

# Log on to Openshift and select project
oc login https://master.ocp.wurnet.nl
oc project glosis

# Register image with Openshift
oc import-image --confirm glosis-fuseki:$1 --from docker-registry.wur.nl/duque004/glosis-fuseki:$1

# Check if Openshift tag was provided and link
if [[ $2 ]] 
  then
  oc tag glosis-fuseki:$1 glosis-fuseki:$2
fi

