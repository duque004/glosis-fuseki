FROM secoresearch/fuseki:latest

# Set environment variables
ENV ADMIN_PASSWORD toto  
ENV ENABLE_DATA_WRITE true 
ENV ENABLE_UPDATE true 
ENV ENABLE_UPLOAD true

# Load datasets
COPY ./MarsSoilSurvey.ttl $FUSEKI_BASE/databases/MarsSoilSurvey.ttl
ENV TDBLOADER $JAVA_CMD tdb.tdbloader --desc=$FUSEKI_BASE/databases/MarsSoilSurvey.ttl

# Load datasets
COPY ./tbd.cfg $FUSEKI_BASE/tbd.cfg

# Set permissions to allow fuseki to run as an arbitrary user
#RUN chgrp -R 0 $FUSEKI_BASE \
#    && chmod -R g+rwX $FUSEKI_BASE

################### Usage #####################

# Build it
# docker build --force-rm -t glosis/fuseki . -f ./Dockerfile 

# Run command
# docker run -dit --name glosis-fuseki -e ADMIN_PASSWORD=toto -p 3030:3030 glosis/fuseki

